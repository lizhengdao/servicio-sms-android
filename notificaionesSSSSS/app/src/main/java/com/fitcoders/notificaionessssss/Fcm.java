package com.fitcoders.notificaionessssss;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class Fcm extends FirebaseMessagingService {
    private final static int REQUEST_CODE_PERMISSION_SEND_SMS = 123;
    private static final String TAG = "asdasd" ;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        Log.d(TAG,"nuevo Token: "+s);
    }



    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Log.d(TAG,"mensaje recibido");
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();

        String numero = data.get("numero");
        String mensaje = data.get("mensaje");
        Log.d(TAG,"mensaje recibido: "+numero+"///"+mensaje);

       /* RemoteMessage.Notification notification = remoteMessage.getNotification();
        String title = notification.getTitle();
        String body = notification.getBody();
        Log.d(TAG,"mensaje recibido: "+title+"///"+body);*/
        //sendNotification(title,body);
       sendSMS(numero,mensaje);

    }

    public void sendSMS(String numero, String mensaje){
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(numero,null,mensaje,null,null);
            //Toast.makeText(getApplicationContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "Mensaje no enviado, datos incorrectos.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void sendNotification(String title, String body) {
        Intent intent = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,MyNotification.NOTIFICATION_ID,intent,PendingIntent.FLAG_ONE_SHOT);

        MyNotification notification = new MyNotification(this,MyNotification.CHANNEL_ID_NOTIFICATIONS);
        notification.build(R.drawable.ic_launcher_background,title,body,pendingIntent);

        notification.addChannel("Notificaciones", NotificationManager.IMPORTANCE_DEFAULT);

        notification.createChannelGroup(MyNotification.CHANNEL_GROUP_GENERAL, R.string.fcm_fallback_notification_channel_label);
        notification.show(MyNotification.NOTIFICATION_ID);
    }

}
